﻿//Задано N точек на плоскости.Указать(N - 1) - звенную несамопересекающуюся замкнутую ломаную, проходящую через все эти точки.

#include <iostream>
#include <vector>
#include <cmath>

struct Point {
	double x;
	double y;
	double sin;

	bool operator<(Point& second) {
		return x < second.x || (x == second.x && y < second.y);
	}

	double FindSin(Point& Start) { // Находим синус угла прямой, построенной на начальной и текущей точках.
		return ((y - Start.y) / sqrt(((y - Start.y) * (y - Start.y) + (x - Start.x) * (x - Start.x))));
	}
};

std::ostream& operator<<(std::ostream& stream, const Point& point) {
	stream << point.x << " " << point.y;
	return stream;
}

Point FindingStartPoint(std::vector<Point>& line) { // Находим точку отсчёта (самую "левую" точку).
	Point Start;
	Start.x = line[0].x;
	Start.y = line[0].y;
	for (int i = 0; i < line.size(); i++) {
		if (line[i] < Start) {
			Start = line[i];
			std::swap(line[0], line[i]); // Ставим точку отсчёта в начало, чтобы потом её не сдвигать (т.к. её синус будет равен нулю и она будет стоять не на своём месте после сортировки).
		}
	}
	return Start;
}

void MakingPolyline(std::vector<Point>& line) {
	Point Start = FindingStartPoint(line);

	for (int i = 1; i < line.size(); i++) { // Сортируем от второй точки, т.к. первая - это точка отсчёта.

		line[i].sin = line[i].FindSin(Start); 
		int j = i;                                                                                                                                     

		while (j > 1 && line[j - 1].sin < line[j].sin) { // Условие было рудиментом (ненужной частью) предыдущей версии кода, которое я забыл убрать.
			std::swap(line[j - 1], line[j]);
			j -= 1;
		}
	}

}


int main() {

	int PointsNumber = 0;
	std::cin >> PointsNumber;

	std::vector<Point> line(PointsNumber);
	for (int i = 0; i < line.size(); i++) {
		std::cin >> line[i].x >> line[i].y;
		line[i].sin = 0;
	}

	MakingPolyline(line);

	for (int i = 0; i < line.size(); i++) {
		std::cout << line[i] << std::endl;
	}

	return 0;
}