﻿/*Группа людей называется современниками если был такой момент, когда они могли собраться вместе.Для этого в этот момент каждому из них должно было уже исполниться 18 лет, но ещё не исполниться 80 лет.
Дан список Жизни Великих Людей.Необходимо получить максимальное количество современников.В день 18летия человек уже может принимать участие в собраниях, а в день 80летия и в день смерти уже не может.
Замечание.Человек мог не дожить до 18 - летия, либо умереть в день 18 - летия.В этих случаях принимать участие в собраниях он не мог.*/

#include <iostream>
#include <vector>
#include <algorithm>

struct Date {

	int Day;
	int Month;
	int Year;

	int Type;

	bool operator>(Date& second) {
		return Year > second.Year
			|| (Year == second.Year && Month > second.Month)
			|| (Year == second.Year && Month == second.Month && Day > second.Day);
	}

	bool operator<(Date& second) {
		return second > *this;
	}

	bool operator== (Date& second) { // Нужно для нахождения событий, произошедших в один день.
		return Year == second.Year && Month == second.Month && Day == second.Day;
	}
};

std::istream& operator>>(std::istream& stream, Date& date) {
	stream >> date.Day >> date.Month >> date.Year;
	return stream;
}

bool IsSomeYearOld(Date birth, Date death, int NumberOfYears) {
	return death.Year - birth.Year > NumberOfYears
		|| (death.Year - birth.Year == NumberOfYears && death.Month - birth.Month > 0)
		|| (death.Year - birth.Year == NumberOfYears && death.Month - birth.Month == 0 && death.Day - birth.Day > 0);
}

int FindingMaximumOverlay(std::vector<Date>& Dates) {

	int MaximumOverlay = 0;
	int Overlay = 0;

	for (int i = 0; i < Dates.size(); i++) {

		do Overlay += Dates[i++].Type; // Добавляем все события, произошедшие в этот день.
		while (i < Dates.size() - 1 && Dates[i + 1] == Dates[i]);
		i--;

		if (Overlay > MaximumOverlay) {
			MaximumOverlay = Overlay;
		}
	}

	return MaximumOverlay;
}

void ChangingDates(std::vector<Date>& Dates, int BirthIndex, int DeathIndex) {
	if (IsSomeYearOld(Dates[BirthIndex], Dates[DeathIndex], 18)) {

		Dates[BirthIndex].Type = +1; //  +1 - новый человек стал современником.
		Dates[DeathIndex].Type = -1; // -1 - человек перестал быть современником.

		if (IsSomeYearOld(Dates[BirthIndex], Dates[DeathIndex], 80)) {// Если человек прожил больше 80 лет, то будем считать, что он он умер в день 80-летия, т.к. после
			Dates[DeathIndex].Day = Dates[BirthIndex].Day;      // этого он больше не будет современником для других.
			Dates[DeathIndex].Month = Dates[BirthIndex].Month;
			Dates[DeathIndex].Year = Dates[BirthIndex].Year + 80;
		}

		Dates[BirthIndex].Year += 18; // Передвигаем дату рождения на 18 лет вперёд, т.к. до этого человек не являлся современником для других.

	}

	else { // Если человек не дожил до 18 лет, то мы обнуляем его даты смерти с рождения, а также тип (чтобы не учитывать их в конечном подсчёте).

		Dates[BirthIndex].Type = 0;
		Dates[BirthIndex].Year = 0;

		Dates[DeathIndex].Year = 0;
		Dates[DeathIndex].Type = 0;

	}
}

int main() {

	int NumberOfPeople;
	std::cin >> NumberOfPeople;
	std::vector<Date> Dates(2 * NumberOfPeople);

	for (int i = 0; i < NumberOfPeople; i++) {
		int BirthIndex = 2 * i;
		int DeathIndex = 2 * i + 1;
		std::cin >> Dates[BirthIndex] >> Dates[DeathIndex];
		ChangingDates(Dates, BirthIndex, DeathIndex);
	}

	std::sort(Dates.begin(), Dates.end());

	std::cout << FindingMaximumOverlay(Dates);

	return 0;
}