﻿/* Мои замеры времени показали, что данная сортировка работает быстрее std::sort всегда, кроме тех ситуаций, когда массив отсортирован в обратную сторону (по убыванию),
Причём только при размере массива, бОльшего 17.000.000. Для этого я создавал вектор случайных чисел (с элементами от 0 до 1.000.000.000) и, если надо было, сортировал до замера.
Не знаю, почему контест выдал столько баллов.
*/

//#include "sort.h"
#include <algorithm>

void InsertionSort(unsigned int* arr, unsigned int right) {

	for (int i = 1; i < right; i++) {

		int j = i;

		while (j >= 1 && arr[j - 1] > arr[j]) {
			std::swap(arr[j - 1], arr[j]);
			j -= 1;
		}

	}
}

int Partition(unsigned int* arr, unsigned int right, unsigned int Border) {

	if (right < Border) { // Если надо отсортировать элементы, количество которых меньше, чем Border - используем сортировку вставками.
		InsertionSort(arr, right + 1);
		return 0;
	}

	int PivotIndex = right / 2; // Контест не давал использовать rand(), поэтому сделал пивот таким.
	const int Pivot = arr[PivotIndex];

	std::swap(arr[PivotIndex], arr[right]);
	int i = 0;

	for (int j = 0; j < right; j++) {
		if (arr[j] <= Pivot) {
			std::swap(arr[j], arr[i++]);
		}
	}

	std::swap(arr[right], arr[i]);

	return i;
}

void CustomSort(unsigned int* arr, unsigned int size, unsigned int Border) {

	int PartitionIndex = Partition(arr, size - 1, Border);

	if (PartitionIndex > 0) {
		CustomSort(arr, PartitionIndex, Border);
	}

	if (PartitionIndex < size - 1) {
		CustomSort(arr + PartitionIndex + 1, size - PartitionIndex - 1, Border);
	}
}

void Sort(unsigned int* arr, unsigned int size) {
	CustomSort(arr, size, size / 400000); // Опытным путём вывел, что 400000 - лучшее значение для больших массивов.
}