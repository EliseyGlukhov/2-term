﻿// Дан массив неотрицательных целых 64 - разрядных чисел.Количество чисел не больше 1000000. Отсортировать массив методом MSD по битам(бинарный QuickSort).

#include <iostream>
#include <vector>

int GetDigit(long long number, int rank) {
	return (number >> (rank - 1)) & 1;
}

int Partition(std::vector<long long>& data, const int& left, const int& right, const int& Rank) {

	if (right <= left) {
		return left;
	}

	int i = right;

	for (int j = right; j >= left; j--) { // Обходим массив справа налево.
		if (GetDigit(data[j], Rank) >= 1) { // Т.к. у нас побитовая сортировка, у нас в первой части элементы вида 0xxxx..., а во второй 1xxxx...
			std::swap(data[j], data[i--]);
		}
	}
	return ++i;
}

int GetRanksCount(long long number) { // Функция возвращает количество двоичных разрядов в числе.
	int RanksCount = 0;

	while (number > 0) {
		RanksCount++;
		number >>= 1;
	}

	return RanksCount;
}

void BinaryMSDSort(std::vector<long long>& data, const int& left, const int& right, const int& Rank) {
	if (Rank <= 0) {
		return;
	}

	int PartitionIndex = Partition(data, left, right, Rank);


	if (PartitionIndex > 1 && Rank > 1) { // Если PartitionIndex <=1, то вызывать BinaryMSDSort от PartitionIndex - 1 нет смысла. Аналогично для Rank <= 1.
		
		BinaryMSDSort(data, left, PartitionIndex - 1, Rank - 1);
	}	

	if (right > PartitionIndex && Rank > 1) { // Если right <= PartitionIndex, то дальше идти нет смысла, т.к. данный отрезок массива мы уже отсортировали.
		BinaryMSDSort(data, PartitionIndex, right, Rank - 1);
	}
}

int main() {

	int MassiveSize = 0;
	std::cin >> MassiveSize;

	std::vector<long long> data(MassiveSize);

	long long Maximum = 0;

	for (int i = 0; i < data.size(); i++) {
		std::cin >> data[i];
		if (data[i] > Maximum) { // Попутно с вводом ищем максимальный элемент.
			Maximum = data[i];
		}
	}

	int MaximumRank = GetRanksCount(Maximum);

	BinaryMSDSort(data, 0, MassiveSize - 1, MaximumRank);

	for (int i = 0; i < MassiveSize; i++) {
		std::cout << data[i] << " ";
	}

	return 0;
}