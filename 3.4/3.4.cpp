﻿/*Даны неотрицательные целые числа n, k и массив целых чисел из диапазона[0..10^9] размера n.
Требуется найти k - ю порядковую статистику.т.е.напечатать число, которое бы стояло на позиции с индексом k ∈[0..n - 1] в отсортированном массиве.
Напишите нерекурсивный алгоритм.
Требования к дополнительной памяти : O(n).
Требуемое среднее время работы : O(n). */

#include <iostream>
#include <vector>

int GenerateRandomPivotIndex(const int& left, const int& right) {
	return left + rand() % (right - left);
}

int Partition(std::vector<int>& Data, const int& left, const int& right) {

	if (right <= left) {
		return left;
	}

	const int PivotIndex = GenerateRandomPivotIndex(left, right);
	const int Pivot = Data[PivotIndex];

	std::swap(Data[PivotIndex], Data[right]);

	int i = right;

	for (int j = right; j >= left; j--) { // Проходим массив справа налево.
		if (Data[j] >= Pivot) {
			std::swap(Data[j], Data[i--]);
		}
	}

	std::swap(Data[right], Data[++i]); // У нас i указывает на элемент, идущий перед первым (относительно начала массива) элементом, бОльшим пивота.
	return i;
}

int FindingOrderStatistic(std::vector<int>& Data, const int& OrderStatistic) {

	int left = 0;
	int right = Data.size() - 1;

	while (left <= right) {

		int PartitionIndex = Partition(Data, left, right);

		if (PartitionIndex == OrderStatistic) {
			return Data[PartitionIndex];
		}
		else {

			if (PartitionIndex > OrderStatistic) {
				right = PartitionIndex - 1; // Сдвигаем правую границу влево, т.к. наша порядковая статистика меньше (левее) PartitionIndex'а.
			}
			else {
				left = PartitionIndex + 1; // Сдвигаем левую границу вправо, т.к.наша порядковая статистика больше (правее) PartitionIndex'а.
			}
		}
	}
}

int main() {

	int number = 0;
	std::cin >> number;

	int OrderStatistic = 0;
	std::cin >> OrderStatistic;

	std::vector<int> Data(number);
	for (int i = 0; i < number; i++) {
		std::cin >> Data[i];
	}

	std::cout << FindingOrderStatistic(Data, OrderStatistic);

	return 0;
}