﻿/*Дана последовательность целых чисел a1...an и натуральное число k, такое что для любых i, j: если j >= i + k, то a[i] <= a[j].
Требуется отсортировать последовательность.Последовательность может быть очень длинной.Время работы O(n * log(k)).Доп.память O(k).Использовать слияние.*/

#include <iostream>
#include <vector>
#include <algorithm>

void Merge(std::vector<int>& data, const long long int& Border, const long long int& End) {

	std::vector<int> NewData;
	long long int i = 0;
	long long int j = Border;

	while (i < Border && j < End) {
		if (data[i] < data[j]) {
			NewData.push_back(data[i++]);
		}
		else {
			NewData.push_back(data[j++]);
		}
	}

	while (j < End) {
		NewData.push_back(data[j++]);
	}

	while (i < Border) {
		NewData.push_back(data[i++]);
	}
	std::swap(data, NewData);
}

void SortAlmostSortedMassive(std::vector<int>& data, const long long int& k, const long long int& MassiveSize) {

	long long int Border = k;

	for (int i = 0; i < k; i++) { // Читаем сначала первые k элементов.
		std::cin >> data[i];
	}

	std::sort(data.begin(), data.begin() + k); // Сортируем сначала первые k элементов.	

	while (Border < MassiveSize) {
		int NextElements = std::min(k, MassiveSize - Border); // NextElements означает, что мы берём MassiveSize - border вместо k, если до конца исходного массива осталось меньше k эл-ов.
		
		for (int i = 0; i < NextElements; i++) { // Читаем следующие k элементов. 
			std::cin >> data[k + i];                             
		}

		std::sort(data.begin() + k, data.begin() + k + NextElements); // Сортируем вторые k элементов 
		Merge(data, k, k + NextElements); // Сливаем текущие k элементов и предыдущие. После сливания первые k элементов в массиве стоят уже на своих
															   // местах, поэтому их можно выписывать.
		if (Border + k < MassiveSize) {
			for (int i = 0; i < k; i++) { // Выводим в массиве первые k элементов (они уже стоят на своих местах) и перемащаем вторые k элементов на место первых.
				std::cout << data[i] << " ";
				data[i] = data[k + i];
			}
		}
		else {
			for (int i = 0; i < k + NextElements; i++) { // Выписываем все оставшиеся элементы (выполняется на последнем шаге).
				std::cout << data[i] << " ";
			}
		}
		Border += k; // Смещаем границу на k, так как предыдущие k элементов уже отсортированы и выведены.
	}
}

int main() {

	long long int MassiveSize = 0;
	std::cin >> MassiveSize;

	long long int k = 0;
	std::cin >> k;

	std::vector<int> data(2 * k); // Будем сортировать массив по 2 * k элементов.
	SortAlmostSortedMassive(data, k, MassiveSize);

	return 0;
}